//
//  NetworkAdaptor.h
//  mosad_hw3
//
//  Created by 邵震东 on 2021/11/9.
//

#import <UIKit/UIKit.h>

@interface NetworkAdaptor : NSObject
-(void) get;
-(void) post;
//@property(nonatomic, strong) NSString *getData;
@property(nonatomic, strong) NSData *data;
@property(nonatomic, strong) NSData *postData;
@property(nonatomic, strong) NSString *crctStr;

@end

