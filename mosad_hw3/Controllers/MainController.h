//
//  MainController.h
//  mosad_hw3
//
//  Created by 邵震东 on 2021/11/8.
//

#ifndef MainController_h
#define MainController_h
#import <UIKit/UIKit.h>
#import "TestController.h"
#import "../Network/NetworkAdaptor.h"

@interface MainController : UIViewController

@end


#endif /* MainController_h */
