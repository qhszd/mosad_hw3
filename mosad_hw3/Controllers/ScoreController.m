//
//  ScoreController.m
//  mosad_hw3
//
//  Created by 邵震东 on 2021/11/10.
//

#import "ScoreController.h"

@interface ScoreController()
{
    int cntr;
}
@property(nonatomic, strong) UIImageView *star1;
@property(nonatomic, strong) UIImageView *star2;
@property(nonatomic, strong) UIImageView *star3;
@property(nonatomic, strong) UIImageView *star4;
@property(nonatomic, strong) UILabel *score;

@end

@implementation ScoreController

-(void) viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor: [UIColor whiteColor]];
    
    self->cntr = 0;
    
    UILabel *ttl = [[UILabel alloc] initWithFrame: CGRectMake(self.view.frame.size.width / 2 - 150, self.view.frame.size.height / 6, 300, 60)];
    [ttl setTextAlignment: NSTextAlignmentCenter];
    [ttl setText: @"分 数"];
    [ttl setFont: [UIFont boldSystemFontOfSize: 50]];
    
    self.score = [[UILabel alloc] initWithFrame: CGRectMake(self.view.frame.size.width / 2 - 150, self.view.frame.size.height / 3, 300, 60)];
    [self.score setTextAlignment: NSTextAlignmentCenter];
    [self.score setText: [NSString stringWithFormat: @"%d", self->cntr]];
    [self.score setFont: [UIFont boldSystemFontOfSize: 40]];
    
    self.star1 = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"myStar"]];
    [self.star1 setFrame: CGRectMake(self.view.frame.size.width / 2 - 130, self.view.frame.size.height / 2, 50, 50)];
    self.star2 = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"myStar"]];
    [self.star2 setFrame: CGRectMake(self.view.frame.size.width / 2 - 60, self.view.frame.size.height / 2, 50, 50)];
    self.star3 = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"myStar"]];
    [self.star3 setFrame: CGRectMake(self.view.frame.size.width / 2 + 10, self.view.frame.size.height / 2, 50, 50)];
    self.star4 = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"myStar"]];
    [self.star4 setFrame: CGRectMake(self.view.frame.size.width / 2 + 80, self.view.frame.size.height / 2, 50, 50)];
    
    UIButton *btn = [[UIButton alloc] initWithFrame: CGRectMake(self.view.frame.size.width / 2 - 150, self.view.frame.size.height - 100, 300, 50)];
    [btn setBackgroundColor: [UIColor systemGreenColor]];
    [btn setTitle: @"返 回" forState: UIControlStateNormal];
    [btn.titleLabel setFont: [UIFont boldSystemFontOfSize: 20]];
    [btn.layer setCornerRadius: btn.layer.frame.size.height / 4];
    [btn addTarget: self action: @selector(clckBack:) forControlEvents: UIControlEventTouchUpInside];
    
    [self.view addSubview: ttl];
    [self.view addSubview: self.score];
    [self.view addSubview: self.star1];
    [self.view addSubview: self.star2];
    [self.view addSubview: self.star3];
    [self.view addSubview: self.star4];
    [self.view addSubview: btn];
}

-(void)viewDidAppear:(BOOL)animated
{
    sleep(0.5);
    [UIView animateKeyframesWithDuration:0.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:0.6 animations: ^{
            // key frame 0
            [self.star1 setFrame: CGRectMake(self.star1.frame.origin.x, self.star1.frame.origin.y, 50, 50)];
        }];
        [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.4 animations: ^{
            // key frame 1
            [self.star1 setFrame: CGRectMake(self.star1.frame.origin.x - 5, self.star1.frame.origin.y - 5, 60, 60)];
        }];
    } completion:^(BOOL finished) {}];
    [UIView animateKeyframesWithDuration:0.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:0.6 animations: ^{
            // key frame 0
            [self.star1 setFrame: CGRectMake(self.star1.frame.origin.x, self.star1.frame.origin.y, 60, 60)];
        }];
        [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.4 animations: ^{
            // key frame 1
            [self.star1 setFrame: CGRectMake(self.star1.frame.origin.x + 5, self.star1.frame.origin.y + 5, 50, 50)];
        }];
    } completion:^(BOOL finished) {
        if ([[self.truthArr objectAtIndex: 0] isEqual: @1])
        {
            [self.star1 setImage: [UIImage imageNamed: @"myStar.fill"]];
            [self.score setText: [NSString stringWithFormat: @"%d", ++self->cntr]];
        }
        [UIView animateKeyframesWithDuration:0.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
            [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:0.6 animations: ^{
                // key frame 0
                [self.star2 setFrame: CGRectMake(self.star2.frame.origin.x, self.star2.frame.origin.y, 50, 50)];
            }];
            [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.4 animations: ^{
                // key frame 1
                [self.star2 setFrame: CGRectMake(self.star2.frame.origin.x - 5, self.star2.frame.origin.y - 5, 60, 60)];
            }];
        } completion:^(BOOL finished) {}];
        [UIView animateKeyframesWithDuration:0.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
            [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:0.6 animations: ^{
                // key frame 0
                [self.star2 setFrame: CGRectMake(self.star2.frame.origin.x, self.star2.frame.origin.y, 60, 60)];
            }];
            [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.4 animations: ^{
                // key frame 1
                [self.star2 setFrame: CGRectMake(self.star2.frame.origin.x + 5, self.star2.frame.origin.y + 5, 50, 50)];
            }];
        } completion:^(BOOL finished) {
            if ([[self.truthArr objectAtIndex: 1] isEqual: @1])
            {
                [self.star2 setImage: [UIImage imageNamed: @"myStar.fill"]];
                [self.score setText: [NSString stringWithFormat: @"%d", ++self->cntr]];
            }
            [UIView animateKeyframesWithDuration:0.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
                [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:0.6 animations: ^{
                    // key frame 0
                    [self.star3 setFrame: CGRectMake(self.star3.frame.origin.x, self.star3.frame.origin.y, 50, 50)];
                }];
                [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.4 animations: ^{
                    // key frame 1
                    [self.star3 setFrame: CGRectMake(self.star3.frame.origin.x - 5, self.star3.frame.origin.y - 5, 60, 60)];
                }];
            } completion:^(BOOL finished) {}];
            [UIView animateKeyframesWithDuration:0.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
                [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:0.6 animations: ^{
                    // key frame 0
                    [self.star3 setFrame: CGRectMake(self.star3.frame.origin.x, self.star3.frame.origin.y, 60, 60)];
                }];
                [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.4 animations: ^{
                    // key frame 1
                    [self.star3 setFrame: CGRectMake(self.star3.frame.origin.x + 5, self.star3.frame.origin.y + 5, 50, 50)];
                }];
            } completion:^(BOOL finished) {
                if ([[self.truthArr objectAtIndex: 2] isEqual: @1])
                {
                    [self.star3 setImage: [UIImage imageNamed: @"myStar.fill"]];
                    [self.score setText: [NSString stringWithFormat: @"%d", ++self->cntr]];
                }
                [UIView animateKeyframesWithDuration:0.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
                    [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:0.6 animations: ^{
                        // key frame 0
                        [self.star4 setFrame: CGRectMake(self.star4.frame.origin.x, self.star4.frame.origin.y, 50, 50)];
                    }];
                    [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.4 animations: ^{
                        // key frame 1
                        [self.star4 setFrame: CGRectMake(self.star4.frame.origin.x - 5, self.star4.frame.origin.y - 5, 60, 60)];
                    }];
                } completion:^(BOOL finished) {}];
                [UIView animateKeyframesWithDuration:0.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
                    [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:0.6 animations: ^{
                        // key frame 0
                        [self.star4 setFrame: CGRectMake(self.star4.frame.origin.x, self.star4.frame.origin.y, 60, 60)];
                    }];
                    [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.4 animations: ^{
                        // key frame 1
                        [self.star4 setFrame: CGRectMake(self.star4.frame.origin.x + 5, self.star4.frame.origin.y + 5, 50, 50)];
                    }];
                } completion:^(BOOL finished) {
                    if ([[self.truthArr objectAtIndex: 3] isEqual: @1])
                    {
                        [self.star4 setImage: [UIImage imageNamed: @"myStar.fill"]];
                        [self.score setText: [NSString stringWithFormat: @"%d", ++self->cntr]];
                    }
                }];
            }];
        }];
    }];
}

-(void) clckBack: (UIButton *) btn
{
    [self.navigationController popToRootViewControllerAnimated: YES];
}

@end
