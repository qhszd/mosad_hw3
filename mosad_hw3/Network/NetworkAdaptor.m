//
//  NetworkAdaptor.m
//  mosad_hw3
//
//  Created by 邵震东 on 2021/11/9.
//

#import "NetworkAdaptor.h"

@interface NetworkAdaptor()
{
    NSString *getURL;
    NSString *postURL;
    NSURLSession *session;
}

@end

@implementation NetworkAdaptor

-(id) init
{
    self = [super init];
    self->getURL = @"http://172.18.178.56:8360/hw3/get_question";
    self->postURL = @"http://172.18.178.56:8360/hw3/query";
    
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
    self->session = [NSURLSession sessionWithConfiguration: conf
                                                  delegate: self
                                             delegateQueue: [NSOperationQueue mainQueue]];

    return self;
}

-(void) get
{
    NSURL *getURL = [[NSURL alloc] initWithString: self->getURL];
    NSURLRequest *req = [NSURLRequest requestWithURL: getURL];
    NSURLSessionDataTask *task = [self->session dataTaskWithRequest: req
                                                  completionHandler: ^(NSData *data,
                                                                       NSURLResponse *rsp,
                                                                       NSError *err) {
        if (err == nil)
        {
            self.data = data;
        }
    }];
    [task resume];
}

-(void) post
{
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData: self.postData options: NSJSONReadingMutableContainers error:nil];
    NSURL *postURL = [[NSURL alloc] initWithString: self->postURL];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL: postURL];
    [req setHTTPMethod: @"POST"];
    [req setHTTPBody: self.postData];
    
    NSURLSessionDataTask *task = [self->session dataTaskWithRequest: req
                                                  completionHandler:^(NSData *data,
                                                                      NSURLResponse *rsp,
                                                                      NSError *error){
        if (error == nil)
        {
//            NSString *text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
            NSDictionary *dictPush = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error:nil];
//            self.rspnsData = data;
//            NSLog(@"%@", dict[@"data"][@"res"]);
            if ([dictPush[@"data"][@"res"] isEqualToString: @"true"])
            {
                self.crctStr = dict[@"choice"];
            }
        }
    }];
    [task resume];
}

@end
