//
//  MainController.m
//  mosad_hw3
//
//  Created by 邵震东 on 2021/11/8.
//

#import "MainController.h"

@interface MainController()
//@property (nonatomic, strong) NSString *getData;
@property (nonatomic, strong) NetworkAdaptor *adptr;
@end

@implementation MainController

- (void) viewDidLoad
{
    [super viewDidLoad];
    [self setTitle: @"图片识别系统"];
    UIButton *btn = [self constructMainButton];
    [btn addTarget:self action: @selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: btn];
    
    self.adptr = [[NetworkAdaptor alloc] init];
    [self.adptr get];
    
    NSLog(@"CHECKPOINT2");
}

- (void) change : (UIButton *) btn
{
    TestController *tc = [[TestController alloc] init];
    [tc setData: self.adptr.data];
    [self setHidesBottomBarWhenPushed: YES];
    [tc.navigationItem setHidesBackButton: YES];
    [self.navigationController pushViewController:tc animated:YES];
    [self setHidesBottomBarWhenPushed: NO];
}

- (UIButton *) constructMainButton
{
    UIButton *btn = [[UIButton alloc] initWithFrame: CGRectMake(self.view.frame.size.width / 2 - 125, self.view.frame.size.height / 2 - 125, 250, 250)];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    CAGradientLayer *gl = [CAGradientLayer layer];
    [gl setFrame: btn.bounds];
    [gl setStartPoint: CGPointMake(0, 0)];
    [gl setEndPoint: CGPointMake(1, 1)];
    [gl setLocations: @[@(0.5), @(1.0)]];
    [gl setColors:@[(id)[[UIColor redColor] CGColor],(id)[[UIColor blueColor] CGColor]]];
    [btn.layer addSublayer: gl];
    [btn.layer setMasksToBounds: YES];
    
    [btn setBackgroundColor: [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 0]];
    [btn.layer setBorderColor: [UIColor systemGray5Color].CGColor];
    [btn.layer setBorderWidth: 2.0];
    [btn setTitle:@"识别" forState:UIControlStateNormal];
    [btn.titleLabel setFont: [UIFont boldSystemFontOfSize: 25]];
    [btn.layer setCornerRadius: btn.layer.frame.size.width / 2];
    return btn;
}


@end
