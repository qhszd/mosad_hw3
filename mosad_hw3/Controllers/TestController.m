//
//  TestController.m
//  mosad_hw3
//
//  Created by 邵震东 on 2021/11/8.
//

#import <unistd.h>
#import "TestController.h"
#import "../Network/NetworkAdaptor.h"
#import "ScoreController.h"

@interface TestController()
{
    unsigned long MAX_SIZE;
    unsigned long CURR_IDX;
    BOOL *truth;
}
@property(nonatomic, strong) UIButton *btn1;
@property(nonatomic, strong) UIButton *btn2;
@property(nonatomic, strong) UIButton *btn3;
@property(nonatomic, strong) UIButton *btnConfirm;
@property(nonatomic, strong) UIButton *btnCntn;
@property(nonatomic, strong) UIImageView *image;
@property(nonatomic, strong) NSDictionary *dict;
@property(nonatomic, strong) UIView *ani;
@property(nonatomic, strong) UILabel *label;
@property(nonatomic, strong) NetworkAdaptor *adptr;

@end

@implementation TestController

- (void) viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor: [UIColor whiteColor]];
    [self setTitle: @"第 1 题"];
    
    self.adptr = [[NetworkAdaptor alloc] init];
    
    NSError *err;
    self.dict = [NSJSONSerialization JSONObjectWithData: self.data options: NSJSONReadingMutableContainers error:&err];
    if (err != nil)
    {
        NSLog(@"err = %@", err);
    }
    NSLog(@"%@", self.dict[@"data"][@"info"][0][@"image"]);
    NSLog(@"%@", self.dict[@"data"][@"info"][0][@"choice1"]);
    NSLog(@"%lu", (unsigned long)[self.dict[@"data"][@"info"] count]);
    
    self->CURR_IDX = 0;
    self->MAX_SIZE = (unsigned long)[self.dict[@"data"][@"info"] count];
    self->truth = (BOOL*)malloc(sizeof(BOOL) * self->MAX_SIZE);
    for ( int i = 0; i < self->MAX_SIZE; ++i )
    {
        truth[i] = NO;
    }
    
    self.image = [[UIImageView alloc] initWithImage: [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString: self.dict[@"data"][@"info"][self->CURR_IDX][@"image"]]]]];
    [self.image setFrame: CGRectMake(self.view.frame.size.width/2 - 100, self.view.frame.size.height/2 - self.view.frame.size.height/4 - 100, 200, 200)];
    
    self.btn1 = [[UIButton alloc] initWithFrame: CGRectMake(self.view.frame.size.width / 2 - 150, self.view.frame.size.height / 2 - 40, 300, 50)];
    [self.btn1 setTitle: self.dict[@"data"][@"info"][self->CURR_IDX][@"choice1"] forState: UIControlStateNormal];
    [self.btn1 setTitleColor: [UIColor blackColor] forState: UIControlStateNormal];
    [self.btn1 setSelected: NO];
    [self.btn1 setTitleColor: [UIColor systemGreenColor] forState: UIControlStateSelected];
    [self.btn1.titleLabel setFont: [UIFont boldSystemFontOfSize: 25]];
    [self.btn1 addTarget: self action: @selector(clck1:) forControlEvents: UIControlEventTouchUpInside];
    [self.btn1.layer setBorderColor: [UIColor systemGreenColor].CGColor];
    [self.btn1.layer setCornerRadius: self.btn1.frame.size.height / 4];
    
    self.btn2 = [[UIButton alloc] initWithFrame: CGRectMake(self.view.frame.size.width / 2 - 150, self.view.frame.size.height / 2 + 40, 300, 50)];
    [self.btn2 setTitle: self.dict[@"data"][@"info"][self->CURR_IDX][@"choice2"] forState: UIControlStateNormal];
    [self.btn2 setTitleColor: [UIColor blackColor] forState: UIControlStateNormal];
    [self.btn2 setTitleColor: [UIColor systemGreenColor] forState: UIControlStateSelected];
    [self.btn2.titleLabel setFont: [UIFont boldSystemFontOfSize: 25]];
    [self.btn2 addTarget: self action: @selector(clck2:) forControlEvents: UIControlEventTouchUpInside];
    [self.btn2.layer setBorderColor: [UIColor systemGreenColor].CGColor];
    [self.btn2.layer setCornerRadius: self.btn2.frame.size.height / 4];
    
    self.btn3 = [[UIButton alloc] initWithFrame: CGRectMake(self.view.frame.size.width / 2 - 150, self.view.frame.size.height / 2 + 120, 300, 50)];
    [self.btn3 setTitle: self.dict[@"data"][@"info"][self->CURR_IDX][@"choice3"] forState: UIControlStateNormal];
    [self.btn3 setTitleColor: [UIColor blackColor] forState: UIControlStateNormal];
    [self.btn3 setTitleColor: [UIColor systemGreenColor] forState: UIControlStateSelected];
    [self.btn3.titleLabel setFont: [UIFont boldSystemFontOfSize: 25]];
    [self.btn3 addTarget: self action: @selector(clck3:) forControlEvents: UIControlEventTouchUpInside];
    [self.btn3.layer setBorderColor: [UIColor systemGreenColor].CGColor];
    [self.btn3.layer setCornerRadius: self.btn3.frame.size.height / 4];
    
    self.btnConfirm = [[UIButton alloc] initWithFrame: CGRectMake(self.view.frame.size.width / 2 - 150, self.view.frame.size.height - 100, 300, 50)];
    [self.btnConfirm setBackgroundColor: [UIColor systemGrayColor]];
    [self.btnConfirm setTitle: @"确 认" forState: UIControlStateNormal];
    [self.btnConfirm.titleLabel setFont: [UIFont boldSystemFontOfSize: 20]];
    [self.btnConfirm.layer setCornerRadius: self.btnConfirm.layer.frame.size.height / 4];
    [self.btnConfirm addTarget: self action: @selector(clckCfrm:) forControlEvents: UIControlEventTouchUpInside];
    
    self.btnCntn = [[UIButton alloc] initWithFrame: CGRectMake(self.view.frame.size.width / 2 - 150, self.view.frame.size.height - 100, 300, 50)];
    [self.btnCntn setBackgroundColor: [UIColor systemGreenColor]];
    [self.btnCntn setTitle: @"继 续" forState: UIControlStateNormal];
    [self.btnCntn.titleLabel setFont: [UIFont boldSystemFontOfSize: 20]];
    [self.btnCntn.layer setCornerRadius: self.btnCntn.layer.frame.size.height / 4];
    [self.btnCntn addTarget: self action: @selector(clckCntn:) forControlEvents: UIControlEventTouchUpInside];
    [self.btnCntn setHidden: YES];
    
    NSDictionary *push = @{@"number": [NSString stringWithFormat: @"%lu", self->CURR_IDX + 1], @"choice": self.btn1.titleLabel.text};
    NSData *pushJSON = [NSJSONSerialization dataWithJSONObject: push options: NSJSONWritingPrettyPrinted error:&err];
    [self.adptr setPostData: pushJSON];
    [self.adptr post];
    push = @{@"number": [NSString stringWithFormat: @"%lu", self->CURR_IDX + 1], @"choice": self.btn2.titleLabel.text};
    pushJSON = [NSJSONSerialization dataWithJSONObject: push options: NSJSONWritingPrettyPrinted error:&err];
    [self.adptr setPostData: pushJSON];
    [self.adptr post];
    push = @{@"number": [NSString stringWithFormat: @"%lu", self->CURR_IDX + 1], @"choice": self.btn3.titleLabel.text};
    pushJSON = [NSJSONSerialization dataWithJSONObject: push options: NSJSONWritingPrettyPrinted error:&err];
    [self.adptr setPostData: pushJSON];
    [self.adptr post];
    
    
    self.ani = [[UIView alloc] initWithFrame: CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height / 4)];
    [self.ani setBackgroundColor: [[UIColor systemGreenColor] colorWithAlphaComponent: 0.6]];
    self.label = [[UILabel alloc] initWithFrame: CGRectMake(10, self.view.frame.size.height, self.view.frame.size.width, 30)];
    [self.label setText: [NSString stringWithFormat: @"正确答案为：%@", self.adptr.crctStr]];
    [self.label setFont: [UIFont boldSystemFontOfSize: [UIFont labelFontSize]]];
    [self.label setTextColor: [UIColor whiteColor]];
    
    [self.view addSubview: self.image];
    [self.view addSubview: self.btn1];
    [self.view addSubview: self.btn2];
    [self.view addSubview: self.btn3];
    [self.view addSubview: self.ani];
    [self.view addSubview: self.label];
    [self.view addSubview: self.btnConfirm];
    [self.view addSubview: self.btnCntn];
}

-(void)clck1 : (UIButton *)btn
{
    [btn setSelected: YES];
    [btn.layer setBorderWidth: 2.0];
    [self.btn2 setSelected: NO];
    [self.btn2.layer setBorderWidth: 0];
    [self.btn3 setSelected: NO];
    [self.btn3.layer setBorderWidth: 0];
    [self.btnConfirm setBackgroundColor: [UIColor systemGreenColor]];
}

-(void)clck2 : (UIButton *)btn
{
    [btn setSelected: YES];
    [btn.layer setBorderWidth: 2.0];
    [self.btn1 setSelected: NO];
    [self.btn1.layer setBorderWidth: 0];
    [self.btn3 setSelected: NO];
    [self.btn3.layer setBorderWidth: 0];
    [self.btnConfirm setBackgroundColor: [UIColor systemGreenColor]];
}

-(void)clck3 : (UIButton *)btn
{
    [btn setSelected: YES];
    [btn.layer setBorderWidth: 2.0];
    [self.btn1 setSelected: NO];
    [self.btn1.layer setBorderWidth: 0];
    [self.btn2 setSelected: NO];
    [self.btn2.layer setBorderWidth: 0];
    [self.btnConfirm setBackgroundColor: [UIColor systemGreenColor]];
}

-(void)clckCfrm : (UIButton *)btn
{
    if (!(self.btn1.selected || self.btn2.selected || self.btn3.selected))
    {
        return;
    }
    
    [UIView animateKeyframesWithDuration:0.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 animations: ^{
            // key frame 0
            [self.ani setFrame: CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height / 4)];
            [self.label setFrame: CGRectMake(10, self.view.frame.size.height + 10, self.view.frame.size.width, 30)];
        }];
        [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.4 animations: ^{
            // key frame 1
            [self.ani setFrame: CGRectMake(0, self.btnCntn.frame.origin.y - 50, self.view.frame.size.width, self.view.frame.size.height / 4)];
            [self.label setFrame: CGRectMake(10, self.btnCntn.frame.origin.y - 40, self.view.frame.size.width, 30)];
        }];
    } completion:^(BOOL finished) {}];
    
    NSString *choice = self.btn1.selected ? self.btn1.titleLabel.text : self.btn2.selected ? self.btn2.titleLabel.text : self.btn3.titleLabel.text;
    
//    NSLog(self.adptr.crctStr);
    if ([choice isEqualToString: self.adptr.crctStr]) {
        NSLog(@"true");
        self->truth[self->CURR_IDX] = YES;
    } else {
        NSLog(@"false");
        [self.btnCntn setBackgroundColor: [UIColor systemRedColor]];
        [self.ani setBackgroundColor:[[UIColor systemRedColor] colorWithAlphaComponent:0.6]];
    }
    [self.label setText: [NSString stringWithFormat: @"正确答案为：%@", self.adptr.crctStr]];
    
    [self.btnConfirm setHidden: YES];
    [self.btnCntn setHidden: NO];
}

-(void)clckCntn : (UIButton *)btn
{
    if (++self->CURR_IDX == self->MAX_SIZE)
    {
        self->CURR_IDX = 0;
        ScoreController *sc = [[ScoreController alloc] init];
        NSLog(@"CHECKPOINT1");
        NSMutableArray *arr = [[NSMutableArray alloc] initWithObjects: @0, @0, @0, @0, nil];
        NSLog(@"CHECKPOINT2");
        for ( int i = 0; i < self->MAX_SIZE; ++i )
        {
            if (self->truth[i]) {
                [arr setObject: @1 atIndexedSubscript: i];
            }
        }
        NSLog(@"CHECKPOINT3");
        sc.truthArr = [[NSArray alloc] initWithArray: arr];
        [self setHidesBottomBarWhenPushed: YES];
        [sc.navigationItem setHidesBackButton: YES];
        [self.navigationController pushViewController:sc animated:YES];
        [self setHidesBottomBarWhenPushed: NO];
        return;
    }
    
    [UIView animateKeyframesWithDuration:0.5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 animations: ^{
            // key frame 0
            [self.ani setFrame: CGRectMake(0, self.btnCntn.frame.origin.y - 50, self.view.frame.size.width, self.view.frame.size.height / 4)];
            [self.label setFrame: CGRectMake(10, self.btnCntn.frame.origin.y - 40, self.view.frame.size.width, 30)];
        }];
        [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.4 animations: ^{
            // key frame 1
            [self.ani setFrame: CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height / 4)];
            [self.label setFrame: CGRectMake(10, self.view.frame.size.height + 10, self.view.frame.size.width, 30)];
        }];
    } completion:^(BOOL finished) {
        [self.ani setBackgroundColor: [[UIColor systemGreenColor] colorWithAlphaComponent: 0.6]];
    }];

    [self.image setImage: [UIImage imageWithData: [NSData dataWithContentsOfURL: [NSURL URLWithString: self.dict[@"data"][@"info"][self->CURR_IDX][@"image"]]]]];
    [self setTitle: [NSString stringWithFormat: @"第 %lu 题", self->CURR_IDX + 1]];
    [self.btn1 setSelected: NO];
    [self.btn1.layer setBorderWidth: 0];
    [self.btn1 setTitle:self.dict[@"data"][@"info"][self->CURR_IDX][@"choice1"] forState: UIControlStateNormal];
    [self.btn2 setSelected: NO];
    [self.btn2.layer setBorderWidth: 0];
    [self.btn2 setTitle:self.dict[@"data"][@"info"][self->CURR_IDX][@"choice2"] forState: UIControlStateNormal];
    [self.btn3 setSelected: NO];
    [self.btn3.layer setBorderWidth: 0];
    [self.btn3 setTitle:self.dict[@"data"][@"info"][self->CURR_IDX][@"choice3"] forState: UIControlStateNormal];
    [self.btnCntn setHidden: YES];
    [self.btnConfirm setBackgroundColor: [UIColor systemGrayColor]];
    [self.btnConfirm setHidden: NO];
    [btn setBackgroundColor: [UIColor systemGreenColor]];
    
    NSDictionary *push = @{@"number": [NSString stringWithFormat: @"%lu", self->CURR_IDX + 1], @"choice": self.btn1.titleLabel.text};
    NSData *pushJSON = [NSJSONSerialization dataWithJSONObject: push options: NSJSONWritingPrettyPrinted error:nil];
    [self.adptr setPostData: pushJSON];
    [self.adptr post];
    push = @{@"number": [NSString stringWithFormat: @"%lu", self->CURR_IDX + 1], @"choice": self.btn2.titleLabel.text};
    pushJSON = [NSJSONSerialization dataWithJSONObject: push options: NSJSONWritingPrettyPrinted error:nil];
    [self.adptr setPostData: pushJSON];
    [self.adptr post];
    push = @{@"number": [NSString stringWithFormat: @"%lu", self->CURR_IDX + 1], @"choice": self.btn3.titleLabel.text};
    pushJSON = [NSJSONSerialization dataWithJSONObject: push options: NSJSONWritingPrettyPrinted error:nil];
    [self.adptr setPostData: pushJSON];
    [self.adptr post];
}

@end
