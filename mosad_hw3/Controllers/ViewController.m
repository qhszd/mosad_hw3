//
//  ViewController.m
//  mosad_hw3
//
//  Created by 邵震东 on 2021/11/4.
//

#import "ViewController.h"
#import "MainController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    MainController *mc = [[MainController alloc] init];
    [self initWithRootViewController: mc];
    [self.view setBackgroundColor: [UIColor whiteColor]];
    
    
//    UILabel *mainTip = [[UILabel alloc] initWithFrame: CGRectMake((self.view.frame.size.width - 200) / 2, 150, 200, 30)];
//    [mainTip setText: @"请选择题目"];
//    [mainTip setTextAlignment: NSTextAlignmentCenter];
//    [mainTip setFont: [UIFont systemFontOfSize: 20]];
//    [self.view addSubview: mainTip];
}


@end
